import sys
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QLineEdit
from PySide2.QtCore import QFile, QIODevice, QTranslator, QObject, QCoreApplication

tr = QObject().tr

def load_css(app, css_path):
    with open(css_path, "r") as f:
            _style = f.read()
            app.setStyleSheet(_style)

def text_changed(text):
    print(tr("Received text : "), text)

def button_clicked(button):
    try:
        print(QCoreApplication.translate("@default","Button Clicked !"))
    except:
        breakpoint()

if __name__ == "__main__":
    from os.path import abspath, dirname
    file_directory = dirname(abspath(__file__))
    translator = QTranslator()
    translator.load(file_directory+'/i18n/fr_FR')

    app = QApplication(sys.argv)
    app.installTranslator(translator)

    ## Adds some styling
    myCssFile = "/style.qss"
    load_css(app, file_directory+myCssFile)

    ## Load UI
    ui_file_name = "form.ui"
    ui_file = QFile(ui_file_name)
    if not ui_file.open(QIODevice.ReadOnly):
        print("Cannot open {}: {}".format(ui_file_name, ui_file.errorString()))
        sys.exit(-1)
    loader = QUiLoader()
    window = loader.load(ui_file)
    ui_file.close()
    if not window:
        print(loader.errorString())
        sys.exit(-1)

    line = window.findChild(QLineEdit, 'lineEdit')
    line.textChanged.connect(text_changed)

    btn = window.findChild(QPushButton, 'pushButton')
    btn.clicked.connect(button_clicked)

    window.show()

    sys.exit(app.exec_())