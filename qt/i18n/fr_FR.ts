<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <location filename="main.py" line="14"/>
        <source>Received text : </source>
        <translation>Texte recu : </translation>
    </message>
    <message>
        <location filename="main.py" line="18"/>
        <source>Button Clicked !</source>
        <translation type="obsolete">On a clické sur le bouton !</translation>
    </message>
</context>
<context>
    <name>QtWidget</name>
    <message>
        <location filename="form.ui" line="20"/>
        <source>QtWidget</source>
        <translation>Widget Mignon</translation>
    </message>
    <message>
        <location filename="form.ui" line="49"/>
        <source>Nom</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="form.ui" line="61"/>
        <source>PushButton</source>
        <translation>Bouton poussoir</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.py" line="18"/>
        <source>Button Clicked !</source>
        <translation type="unfinished">On a clické sur le bouton !</translation>
    </message>
</context>
</TS>
