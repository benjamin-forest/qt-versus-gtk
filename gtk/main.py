import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from os.path import abspath, dirname

import gettext
import locale

def load_css(css_path):
    '''loads a css file globally'''
    provider = Gtk.CssProvider()
    display = Gdk.Display.get_default()
    screen = Gdk.Display.get_default_screen(display)
    Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    provider.load_from_path(css_path)

def text_changed(widget):
    print(_("Received text : "), widget.get_text())

def button_clicked(button):
    print(_("Button Clicked !"))

def load_lang(localedir, lang):
    '''localedir : absolute path of directory where your language files are,  ex /usr/share/locale'''
    # This part for Gtk.Builder .glade files translation
    lang_to_locale={
        'fr':'fr_FR.utf8',
        'en':'en_US.utf8'
    }
    locale.bindtextdomain('myapp', localedir)
    locale.setlocale(locale.LC_ALL, lang_to_locale[lang])
    # This part for python strings translation
    tr_en = gettext.translation('myapp', localedir=localedir, languages=[lang])
    tr_en.install()

if __name__ == "__main__":
    file_directory = dirname(abspath(__file__))
    load_lang(file_directory+"/locale/", "en")

    handlers = {"text_changed":text_changed, "button_clicked":button_clicked}
    builder = Gtk.Builder()
    builder.set_translation_domain('myapp')
    builder.add_from_file("form.glade")
    builder.connect_signals(handlers)
    ## Adds some styling
    myCssFile = "/style.css"
    load_css(file_directory+myCssFile)

    window = builder.get_object("main_window")
    window.show_all()

    Gtk.main()